cmake_minimum_required(VERSION 2.8.3)
project(atar)

#set(CMAKE_BUILD_TYPE Debug)
set(CMAKE_CXX_STANDARD 14)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
        roscpp
        rospy
        std_msgs
        sensor_msgs
        tf_conversions
        cv_bridge
        image_transport
        geometry_msgs
        custom_msgs
        custom_conversions
        )

catkin_package()

find_package(OpenCV REQUIRED)
find_package(Boost REQUIRED)

include_directories(
        include/
        ${catkin_INCLUDE_DIRS}
        )


##########################################################################
#                    Build Calibration nodes
##########################################################################


add_executable(extrinsic_calib_charuco
        src/extrinsic_calib_aruco/main_extrinsic_charuco.cpp
        src/ar_core/IntrinsicCalibrationCharuco.cpp
        src/ar_core/IntrinsicCalibrationCharuco.h)
target_link_libraries(extrinsic_calib_charuco
        ${catkin_LIBRARIES}  ${OpenCV_LIBRARIES})

add_executable(create_aruco_board
        src/utils/create_aruco_board.cpp)
target_link_libraries(create_aruco_board ${OpenCV_LIBRARIES})

add_executable(create_charuco_board
        src/utils/create_charuco_board.cpp)
target_link_libraries(create_charuco_board ${OpenCV_LIBRARIES})

add_executable(arm_to_world_calibration
        src/arm_to_world_calibration/main_arm_to_world.cpp
        src/arm_to_world_calibration/ArmToWorldCalibration.cpp
        src/arm_to_world_calibration/ArmToWorldCalibration.h)
target_link_libraries(arm_to_world_calibration
        ${catkin_LIBRARIES}
        ${OpenCV_LIBRARIES})

add_library(ExtrinsicCalibArucoNodelet
        src/extrinsic_calib_aruco/ExtrinsicArucoNodelet.cpp
        src/extrinsic_calib_aruco/BoardDetector.cpp)
target_link_libraries(ExtrinsicCalibArucoNodelet
        ${catkin_LIBRARIES} ${OpenCV_LIBRARIES})

##########################################################################
#                           Build Common Nodes
##########################################################################
add_executable(
        stereo_image_view
        src/stereo_image_view/main_stereo_image_view.cpp)

add_executable(
        stereo_image_split
        src/stereo_image_view/main_stereo_image_split_TEMPORARY.cpp)

add_executable(stereo_usb_cam_publisher
        src/stereo_usb_cam_publisher/main_stereo_usb_cam_publisher.cpp)

set(executables
        stereo_image_view
        stereo_image_split
        stereo_usb_cam_publisher
        )

foreach (_ex ${executables})
    target_link_libraries(
            ${_ex}
            ${OpenCV_LIBRARIES}
            ${catkin_LIBRARIES})
endforeach ()




##########################################################################
#                           Install Targets
##########################################################################

install(TARGETS
        ExtrinsicCalibArucoNodelet
        ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
        LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
        RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION})




##########################################################################
#                           FlyCapture SDK
##########################################################################

#option(WITH_FlyCapture "Enable support for the FlyCapture SDK" OFF)
#
#if (WITH_FlyCapture)
#    # Find the Flycapture include files and libraries
#    set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} /usr/src/flycapture)
#
#    find_package(Flycapture2)
#    if (FLYCAPTURE2_FOUND)
#        MESSAGE("FLYCAPTURE2 found.")
#        include_directories(${FLYCAPTURE2_INCLUDE_DIR})
#
#        add_executable(camera_capture_flea3
#                src/camera_capture_flea3/main_camera_capture_flea3.cpp
#                src/camera_capture_flea3/Fla3Camera.cpp
#                src/camera_capture_flea3/Fla3Camera.h
#                )
#        target_link_libraries(camera_capture_flea3
#                ${FLYCAPTURE2_LIBRARIES}
#                )
#        target_link_libraries(camera_capture_flea3
#                ${OpenCV_LIBRARIES}
#                ${catkin_LIBRARIES}
#                arucoUtils
#                )
#    endif (FLYCAPTURE2_FOUND)
#endif (WITH_FlyCapture)

